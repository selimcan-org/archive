#lang pollen

@title{Аю, бабай, төлке}

@p{Борын заманда бер карт яшәгән, ди. Көннәрдән-беркөнне шалкан чәчеп торганда
карт янына Аю килеп чыга, ди.}

@p{— Нишлисең? — ди Аю.}

@p{— Шалкан чәчәм,— ди карт.}

@p{— Әйдә, уртагына чәчик,— ди.}

@p{— Әйдә,— ди карт.}

@p{Бергәләп шалкан чәчәргә тотыналар, ди, болар. Карт агач төбен казып тора, Аю
каерып алып тора. Шулай итеп тиз генә җирне әрчеп, шалканны чәчеп тә куялар,
ди.}

@p{Шалкан җитешкәч, Аюга әйтә карт.}

@p{— Ничек бүләбез? Син төбен аласыңмы, сабагынмы? — ди.}

@p{— Мин сабагын алам,— ди Аю.}

@p{Ярар, аюга шалканның яфракларын кисеп бирә дә, бу үзе, унбиш ат йөгенә төяп,
шалканны алып кайтып китә.}

@p{Бер заманны шалканны төяп базарга китеп барганда Аю очрый моңа.}

@p{— Кая барасың? — ди Аю.}

@p{— Базарга шалкан сатарга барам,— ди дә Аюга ике шалкан ыргыта. Аю шалканны
ашап карагач әйтә:}

@p{— Ай-һай, бабай, мине алдагансың бит син, ди. Икенче уртакка эшли калсам,
төбен алам инде,— ди.}

@p{Икенче ел килеп җитә. Карт кырга барган икән, әлеге Аю тагын моның янына
килеп җитә.}

@p{— Бабай, нишлисең? — ди Аю.}

@p{— Бодай чәчәм,— ди.}

@p{— Әйдә уртакка эшлик,— ди Аю.}

@p{— Әйдә,— ди карт.}

@p{Шуннан тотыналар болар бодай чәчәргә. Карт чәчеп тора, Аю койрыгы белән
себереп кенә тора.}

@p{Бодай өлгерә, урып алыр вакыт та килеп җитә. Аю җәй буена бодай кырын саклап
яткан була. Карт бөтен семьясы белән урак урырга китә. Шул вакытны Аю да моның
янына килеп:}

@p{— Син нишлисең, Аю дус, астын аласыңмы, сабагын аласыңмы? — ди карт.}

@p{— Мин быел астын алам инде,— ди Аю.}

@p{— Ярый,— ди карт,— син астын алсаң, мин сабагын алырмын,— ди.}

@p{Карт бодайны урып, сугып, тегермәндә он ясап, күмәчен дә ашап карый.}

@p{Бервакытны он төяп базарга барганда Аю очрый моңа. Карт: «Мә сиңа бергә иккән
игеннең күмәче»,— дип, ярты күмәч тоттырып китә. Күмәчне ашап карагач, Аюның
тагын да ачуы килә. Ачудан Аю: «Алдаган өчен мин хәзер бу картны үтерәм»,
ди. Аюның бу сүзен Төлке ишетә дә картка килеп әйтә, «Аю шулай-шулай дип әйтә»
дип. Аннан Төлке әйтә:}

@p{— Бу хәбәрне белдергән өчен син миңа нәрсә бирәсең? — ди.}

@p{— Тавык бирермен,— ди карт. Ну, ди, элек безгә Аюның үзен үтерергә кирәк,
шуның хәйләсен табыйк,— ди.}

@p{Төлке:}

@p{— Мин Аюның кайда ятканын белеп килим,— ди,— аннан соң ничек үтерү турында
уйлашырбыз,— ди.}

@p{Берничә вакыттан соң Төлке, Аюның оясын белеп, картка килеп әйтә.}

@p{Карт аучыларны җыя да Аюның оясына алып бара. Шулай итеп алар Аюны атып
үтерәләр. Шуннан соң озак та үтми, Төлке яңадан карт янына килеп җитә.}

@p{— Ну, бабай, ди, мин сиңа Аюны үтерергә ярдәм иттем, моның өчен син миңа
нәрсә бирәсең? — ди.}

@p{— Тавык,— ди карт.}

@p{— Соң ул тавыкны каян алабыз? — ди.}

@p{— Әйдә минем белән арбага утыр да безгә бар, тавыкны биреп җибәрермен,— ди.}

@p{— Юк,— ди Төлке,— авылга кайтсам, мине этләр күреп харап итәрләр,— ди.}

@p{— Мин этләргә харап итәргә бирмәм, арбага чыбыклар салып, мин сине чыбыклар
астына яшереп алып кайтырмын,— ди.}

@p{Болай дигәч, Төлке дә риза була. Шуннан соң карт Төлкене арбадагы чыбыклар
астына яшереп өйгә алып кайтып китә. Ишегалдына килеп керүгә атны туктатып,
чылбырдагы этне ычкындырып, арбаны аударып җибәрүгә чыбык астыннан Төлке килеп
чыга. Эт Төлкене күреп тотына куарга. Әй куа, әй куа, куа торгач Төлке, койма
аркылы сикереп, урманга кача. Урманга барып җитеп, үзенең куышына кереп яткач,
алгы аякларыннан сорый икән: «Эттән качканда син нишләдең?» — дип. Алгы аягы
әйтә икән: «Мин колактан уздырыйм дип сикердем»,— ди икән. Арт аягыннан сорый
икән: «Син нишләдең?» — дип. «Мин алгы аяклардан уздырыйм дип сикердем»,— ди
икән. Койрыгыннан сорый: «Син нишләдең?» — дип. Койрык: «Мин этләр тотсын дип
селкендем»,— ди икән. Ул арада куып килгән эт, Төлке артыннан килеп җитеп,
койрыкның селкенгәнен күреп, Төлкене тотып та ала, шуның белән әкият тә бетә.}

@(textology)