#lang pollen

@title{Песнәк белән Күке}

@p{Су читендәге ялгыз имәнгә Песнәк оя ясаган. Шунда ул йомырка салган,
балаларын чыгарган. Песнәк балаларына бөҗәкләр ташыган. Ә балалары Песнәкне,
авызларын ачып, шатлана-шатлана каршы алганнар, ди.}

@p{Бервакыт Песнәк бөҗәк алып кайтса, шундый күренешне күргән: бер баласы су
өстенә егылып төшкән дә, ди, канатлары белән җилпенеп, читкә чыгарга азаплана,
ди. Ә калган балалары чыр-чу килеп кычкырышалар, ди.}

@p{Ни эшләргә белми аптырап калган Песнәк, суга батып барган баласы тирәсендә
оча башлаган, өзгәләнеп кычкырган.}

@p{Шунда очып барган бер Күке:}

@p{— И ахмак,— дигән Песнәккә,— болай булса син барлык балаларыңнан да колак
кагарсың!}

@p{Песнәкнең ачуы килгән дә:}

@p{— Үз балаңны үзең танымыйсыңмы? Минем ояма син салып киткән йомыркадан чыккан
ятим бит ул! — дип җавап биргән дә су өстендәге талга ябышырга өлгергән баласын
коткара башлаган, ди.}

@(textology)
