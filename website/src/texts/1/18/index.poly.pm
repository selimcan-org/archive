#lang pollen

@title{Кәҗә белән Сарык}

@p{Элекке заманда булган икән, ди, бер карт белән карчык. Аларның булган бер
атлары, бер үгезләре. Вак малдан бер Кәҗә белән бер Сарыклары булган, ди.}

@p{Карт бик әрсез, бертуктаусыз эшкә йөри торган бер кеше булган, ди. Ул
көне-төне Үгезне эшкә җигә икән, бер дә ял юк Үгезгә. Үгез, эшкә йөрүдән гаҗиз
булып, авырая башлаган.}

@p{Тота да Үгезне суя карт. Үгезне суйгач, Ат та уйлый башлый: «Миңа да чират
җитәр, ахры»,— дип. Элекке заманда крестьянның тормышы авыр бит ннде. Бу Картны
да ярлылык басканнан-баса бара. Акча кирәк була да Атны да сата бу.}

@p{Инде моның кала хәзер бер Кәҗәсе белән бер Сарыгы гына. Карт белән карчык
үзара киңәш итәләр: «Үгезне суйдык, Атны саттык, терлек асрап торып булмас,
ахры, Кәҗә белән Сарыкны суярга кирәк», диләр. Кәҗә белән Сарык бу сүзләрне
ишетәләр дә үзара сөйләшәләр: «Безгә дә чират җитәр, ахры, Үгезне суйды, Атны
сатты, әйдә без качыйк булмаса»,— диләр. Иртә белән хуҗалары йокыдан торганчы
чыгып сызалар болар. Кәҗә, мөгезенә эләктереп, бер капчык та алып чыгып киткән,
ди.}

@p{Баралар-баралар болар, бара торгач, көн кичкә таба әйләнеп, караңгы да төшә
башлый.}

@p{Бик калын бер урман була. Урман буенда куак төбеннән бер бүре башы табалар
да: «Кирәк булыр әле!» — дип, бүре башын капчыкка салып алып китәләр болар.}

@p{Урман буйлап бара торгач, болар бер ут яктысы күрәләр. Китәләр болар шул ут
яктысына таба. Ут янына барсалар, анда ун Бүре утыра. Бу Бүреләр күп итеп ботка
пешергәннәр дә, шуңа май салып ашап утыралар икән, ди. Кәҗә белән Сарыкны
күргәч, Бүреләрнең күзләре кыза. Бер Бүре торып әйтә: «Безгә ризык та үз аягы
белән килде, бу Ҝәҗә белән Сарыкны буткадан соң тотып ашарбыз»,— ди.}

@p{Кәҗә эшнең кай тирәдә йөргәнен шунда ук аңлап ала да, әче тавыш белән
кычкырып та җибәрә: «Ики-ки, мики-ки, капчыктагы бүре башы бит уники!» —
дип. Шуннан соң Кәҗә капчыктагы бүре башын чыгарып саный башлый: «Бер бүре башы,
ике бүре башы, өч бүре башы»,— дип, шул бер бүре башын әйләндерә дә саный,
әйләндерә дә саный.}

@p{Бүреләр бик каты куркуга төшәләр. Шуннан соң араларындагы бер зурраклары
әкрен генә торып читкә сыза. Аны көтәләр-көтәләр дә, ул кайтмагач, икенчесе,
өченчесе. Шулай акрын гына таялар болар. Шулай итеп, Бүреләр бер-бер артлы
барысы да китеп югала, ди. Кәҗә белән Сарык туйганчы ботка ашап, ут янында ятып
йоклыйлар, ди. Иртә белән торып: «Болай качып йөреп булмас, безне генә асрарлар
әле»,— дип уйлап, хуҗалары янына кайталар. Әле һаман шулай бергә яшиләр икән,
ди, болар.}

@(textology)