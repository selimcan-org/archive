<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/styles.css">
    <script type="text/javascript">
      (function(c,l,a,r,i,t,y){
          c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
          t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
          y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
      })(window, document, "clarity", "script", "okxc5m7729");
    </script>
  </head> 
  <body>
    <h1 style="text-align:
	       center"><a href="https://archive.selimcan.org">Сәлимҗан
	китапханәсе</a></h1>
    <ul>
      <li style="display: inline"><a href="/authors/">Авторлар</a></li>
      <li style="display: inline"><a href="/texts/">Әсәрләр</a></li>
      <li style="display: inline"><a href="/books/">Китаплар</a></li>      
    </ul>
    @(->html doc)
  </body>
</html>
